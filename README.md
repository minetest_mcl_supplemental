# Supplemental blocks for MineClone 2 [`mcl_supplemental`]
This mod adds a few additional building and decorationsl blocks for MineClone 2,
mostly walls.

This should give you more freedom to build your cool buildings.

Version: 0.4.0

The following blocks are added:

## Walls

* Prismarine Wall
* Sandstone Wall
* Red Sandstone Wall
* End Stone Wall
* Netherrack Wall

## Slabs and stairs

* Chiseled Stone Brick Slab
* Prismarine Stair
* Prismarine Slab

## Licensing
Everything in this mod is released under the MIT License.
