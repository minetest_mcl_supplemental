mcl_walls.register_wall("mcl_supplemental:sandstonewall", "Sandstone Wall", "group:sandstone", {"mcl_core_sandstone_top.png", "mcl_core_sandstone_bottom.png", "mcl_core_sandstone_normal.png"})

mcl_walls.register_wall("mcl_supplemental:redsandstonewall", "Red Sandstone Wall", "group:redsandstone", {"mcl_core_red_sandstone_top.png", "mcl_core_red_sandstone_bottom.png", "mcl_core_red_sandstone_normal.png"})

mcl_walls.register_wall("mcl_supplemental:endstonewall", "End Stone Wall", "mcl_end:end_stone", minetest.registered_nodes["mcl_end:end_stone"].tiles)

mcl_walls.register_wall("mcl_supplemental:netherrackwall", "Netherrack Wall", "mcl_nether:netherrack", minetest.registered_nodes["mcl_nether:netherrack"].tiles)

mcl_walls.register_wall("mcl_supplemental:prismarinewall", "Prismarine Wall", "mcl_ocean:prismarine", minetest.registered_nodes["mcl_ocean:prismarine"].tiles)

