mcl_stairs.register_stair_and_slab_simple("prismarine", "mcl_ocean:prismarine", "Prismarine Stairs", "Prismarine Slab", "Double Prismarine Slab")
mcstair.add("mcl_stairs:stair_prismarine")

mcl_stairs.register_slab("stonebrickcarved", "mcl_core:stonebrickcarved",
		{pickaxey=1},
		{"mcl_core_stonebrick_carved.png", "mcl_core_stonebrick_carved.png", "mcl_supplemental_stonebrick_carved_slab.png"},
		"Chiseled Stone Brick Slab",
		mcl_sounds.node_sound_stone_defaults(),
		minetest.registered_nodes["mcl_core:granite_smooth"]._mcl_hardness,
		"Double Chiseled Stone Brick Slab")

-- TODO: Chiseled stone brick stairs

